from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login_account/', views.login_account, name='login_account'),
    path('logout_account/', views.logout_account, name='logout_account')
]


