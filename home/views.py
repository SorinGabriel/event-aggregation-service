from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import HttpResponseRedirect
from django.http import JsonResponse

def index(request):
    template = loader.get_template('home/index.html')
    return HttpResponse(template.render({}, request))


def login_account(request):
    if request.method == 'POST':
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            return JsonResponse({
                'success': True,
                'data': {
                    'url': 'my-account/'
                }
            })
        else:
            return JsonResponse({
                'success': False,
                'message': 'Invalid username or password!'
            })


def logout_account(request):
    logout(request)
    return HttpResponseRedirect('/')
