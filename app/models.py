from django.db import models
from django.core.validators import MinValueValidator


class UserDetails(models.Model):
    ud_id = models.AutoField(primary_key=True)
    ud_user_id = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE
    )
    ud_gender = models.CharField(blank=False, max_length=200)
    ud_birth_date = models.DateField(blank=False, null=True)
    ud_address = models.CharField(blank=True, max_length=200)
    ud_phone_number = models.CharField(blank=False, max_length=200)
    ud_profile_picture = models.CharField(blank=True, max_length=200)


class Event(models.Model):
    readonly_fields = ["event_organizer_user_id"];

    event_id = models.AutoField(primary_key=True)
    event_name = models.CharField(blank=False, max_length=200)
    event_start_datetime = models.DateTimeField(blank=False)
    event_end_datetime = models.DateTimeField(blank=False)
    event_description = models.TextField(blank=True)
    event_image = models.CharField(blank=False, max_length=200)
    event_max_users = models.IntegerField(blank=False, validators=[MinValueValidator(1)])
    event_organizer_user_id = models.ForeignKey(
        'auth.User',
        on_delete=models.DO_NOTHING
    )

    class Visibility(models.IntegerChoices):
        PUBLIC = 1
        PRIVATE = 2

    event_visibility = models.IntegerField(choices=Visibility.choices, default=Visibility.PUBLIC)


class EventAssignedUsers(models.Model):
    eau_id = models.AutoField(primary_key=True)
    eau_user_id = models.ForeignKey(
        'auth.User',
        on_delete=models.DO_NOTHING
    )
    eau_event_id = models.ForeignKey(
        Event,
        on_delete=models.DO_NOTHING
    )

    class Status(models.IntegerChoices):
        PENDING = 1
        ACCEPTED = 2
        DECLINED = 3

    eau_status = models.IntegerField(blank=False, choices=Status.choices, default=Status.PENDING)
