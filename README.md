# Event Aggregation Service

Website that allows organizers to enter events and collect entries for them. 

The website is also equipped with an event search engine and an API, that allows the presentation on other pages/ services.


Features: 
- User Registration and Login (TO DO). User Types: 
    - Admin: has full access (e.g: Add/ Delete/ Edit staff, events)
    - Staff: has access to Add/ Delete/ Edit events)
    - End User: clients/ participants

- Commenting on events by logged in users (NICE TO HAVE) 
- Signing up for events (TO DO)
    - End users will be able to attend or leave an event. 
    - Validation: users can not attent/ leave past events. 
- Event Search Engine (TO DO)
    - Main page will be an events list page with multiple tabs (Upcoming Events, Past Events). Each tab will have a Search functionality. 
- API for other websites/ services that want to present events. (NICE TO HAVE) 



Project setup: 



Useful commands:
# build image (should be run before docker-compose up if you change the Dockerfile)
docker-compose build

# activate image 
docker-compose up 

# stop image
docker-compose down

# list all containers 
docker ps -a   

# list running containers 
docker container ls 

